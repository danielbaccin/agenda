package br.com.caleum.cadastro.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.fragment.MapaFragment;
import br.com.caleum.cadastro.mapa.AtualizadorDePosicao;

public class MostraAlunosProximosActivity extends FragmentActivity{
	
	private AtualizadorDePosicao atualizadorDePosicao;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.map_layout);
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		MapaFragment mapaFragment = new MapaFragment();
		transaction.replace(R.id.mapa,  mapaFragment);
		transaction.commit();
		
		atualizadorDePosicao = new AtualizadorDePosicao(this, mapaFragment);
		
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		atualizadorDePosicao.cancelar();
	}

}
