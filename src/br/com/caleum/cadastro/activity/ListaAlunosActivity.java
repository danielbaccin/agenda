package br.com.caleum.cadastro.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.adapter.ListaAlunosAdapter;
import br.com.caleum.cadastro.dao.AlunoDAO;
import br.com.caleum.cadastro.model.Aluno;
import br.com.caleum.cadastro.model.Extras;
import br.com.caleum.cadastro.task.EnviaAlunoTask;

public class ListaAlunosActivity extends Activity {

	private ListView lista;
	private Aluno aluno;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listagem_alunos);
		
		lista = (ListView) findViewById(R.id.listagem);
		
		registerForContextMenu(lista);
		
		lista.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				aluno = (Aluno) parent.getItemAtPosition(position);
				
				Intent irParaFormulario = new Intent(ListaAlunosActivity.this, FormularioActivity.class);
				irParaFormulario.putExtra(Extras.ALUNO_SELECIONADO, aluno);
				
				
				startActivity(irParaFormulario);
				
			}
		});
		
		lista.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
				
				aluno = (Aluno) parent.getItemAtPosition(position);
				
				return false;
			}
		});
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
									ContextMenuInfo menuInfo) {
		
		MenuItem ligar = menu.add("Ligar");
		ligar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Intent irParaTelefone = new Intent(Intent.ACTION_CALL);
				Uri numeroDoTelefone =  Uri.parse("tel:"+aluno.getTelefone());
				irParaTelefone.setData(numeroDoTelefone);
				startActivity(irParaTelefone);
				
				return false;
			}
		});
		
		
		MenuItem irParaSite = menu.add("Navegar no site");
		irParaSite.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Intent irParaSite = new Intent(Intent.ACTION_VIEW);
				Uri ulrDoSite = Uri.parse("http://"+aluno.getSite());
				irParaSite.setData(ulrDoSite);
				startActivity(irParaSite);
				return false;
			}
		});
		
		
		MenuItem enviarSMS = menu.add("Enviar SMS");
		enviarSMS.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Intent irParaSMS = new Intent(Intent.ACTION_VIEW);
				Uri numeroDoTelefoneParaEnviar = Uri.parse("sms:"+aluno.getTelefone()) ;
				irParaSMS.setData(numeroDoTelefoneParaEnviar);
				irParaSMS.putExtra("sms_body", "enviando sms de teste");
				startActivity(irParaSMS);
				return false;
			}
		});
		
		
		MenuItem email = menu.add("Enviar E-mail");
        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        intentEmail.setType("message/rtc822");
        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {"daniel.baccin@gmail.com"});
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Testando subject do email");
        intentEmail.putExtra(Intent.EXTRA_TEXT, "Testando corpo do email");
        email.setIntent(intentEmail);
		
		MenuItem acharNoMapa = menu.add("Achar no mapa");
		acharNoMapa.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Intent irParaMapa = new Intent(Intent.ACTION_VIEW);
				
				Uri localizacao = Uri.parse("geo:0,0?z=14&q="+aluno.getEndereco());
				irParaMapa.setData(localizacao);
				startActivity(irParaMapa);
				return false;
			}
		});
		
		
		MenuItem deletar = menu.add("Deletar");
		deletar.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				AlunoDAO alunoDAO = new AlunoDAO(ListaAlunosActivity.this);
				
				alunoDAO.deletar(aluno);
				alunoDAO.close();

				carregaLista();
				
				return false;
			}
		});
		
		
		
		super.onCreateContextMenu(menu, v, menuInfo);
		
	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.lista_alunos , menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		carregaLista();
	}

	private void carregaLista() {
		AlunoDAO dao = new AlunoDAO(this);
		List<Aluno> alunos =  dao.getLista();
		dao.close();
		
		ListaAlunosAdapter adapter2 = new ListaAlunosAdapter(this, alunos);
		
		lista.setAdapter(adapter2);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemClicado = item.getItemId();
		switch (itemClicado) {
		case R.id.novo:
			Intent irParaFormulario = new Intent(this, FormularioActivity.class);
			startActivity(irParaFormulario);
			return false;

		case R.id.sincronizar:
			EnviaAlunoTask enviaAlunoTask = new EnviaAlunoTask(this);
			enviaAlunoTask.execute();
			return false;
					
		case R.id.baixarProvas:
			Intent provas = new Intent(this, ProvasActivity.class);
            startActivity(provas);
            return false;
            
		case R.id.verNoMapa:
			Intent verNoMapa = new Intent(this, MostraAlunosProximosActivity.class);
			startActivity(verNoMapa);
			return false;
            
		default:
            return super.onOptionsItemSelected(item);
		}
		
	}
	
}
