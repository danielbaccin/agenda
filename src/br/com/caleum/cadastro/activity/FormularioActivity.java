package br.com.caleum.cadastro.activity;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.dao.AlunoDAO;
import br.com.caleum.cadastro.helper.FormularioActivityHelper;
import br.com.caleum.cadastro.model.Aluno;
import br.com.caleum.cadastro.model.Extras;

public class FormularioActivity extends Activity{
	
	protected static final String MEDIA_TYPE_IMAGE = null;
	private FormularioActivityHelper formularioActivityHelper;
	AlunoDAO dao;
	private String caminhoParaArquivo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.formulario);
		
		Intent intent = getIntent();
		final Aluno alunoParaEdicao = (Aluno) intent.getSerializableExtra(Extras.ALUNO_SELECIONADO);
		
		formularioActivityHelper = new FormularioActivityHelper(this);
		
		Button botao = (Button) findViewById(R.id.inserir);
		
		if(alunoParaEdicao != null){
			botao.setText("Alterar");
			formularioActivityHelper.populaFormularioComAluno(alunoParaEdicao);
		}
		
		botao.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dao = new AlunoDAO(FormularioActivity.this);
				Aluno aluno = formularioActivityHelper.obtemAlunoDoFormulario();
				
				if(alunoParaEdicao == null){
					dao.salva(aluno);
				}else{
					aluno.setId(alunoParaEdicao.getId());
					dao.edita(aluno);
					
				}
				finish();
			}
		});
		
		ImageView foto = formularioActivityHelper.getFoto();
		foto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent irPAraCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				
				caminhoParaArquivo =  Environment.getExternalStorageDirectory().toString() + "/IMG_"+System.currentTimeMillis() +".png";
				
				File arquivo = new File(caminhoParaArquivo);
				Uri localDaImagem = Uri.fromFile(arquivo);
				irPAraCamera.putExtra(MediaStore.EXTRA_OUTPUT, localDaImagem);
				
				startActivityForResult(irPAraCamera, 123);
				
			}
		});
		
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==123){
			if(resultCode==Activity.RESULT_OK){
				formularioActivityHelper.carregaImagem(caminhoParaArquivo);
			}else{
				caminhoParaArquivo = null;
			}
		}
	}
	

}
