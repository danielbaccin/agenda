package br.com.caleum.cadastro.util;

import java.io.IOException;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

public class Localizador {
	
	private Context context;
	
	public Localizador(Context context){
		this.context = context;
	}
	
	
	public LatLng getCoordenada(String endereco) {
		Geocoder geocoder = new Geocoder(context);
		try {
			List<Address> fromLocationName = geocoder.getFromLocationName(endereco, 1);
			if(!fromLocationName.isEmpty()){
				Address address = fromLocationName.get(0);
				return  new LatLng(address.getLatitude(), address.getLongitude());
			}else{
				return null;
			}
		} catch (IOException e) {
			return null;
		}
    }

}
