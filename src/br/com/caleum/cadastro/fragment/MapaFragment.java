package br.com.caleum.cadastro.fragment;

import java.util.List;

import android.content.Context;
import br.com.caleum.cadastro.dao.AlunoDAO;
import br.com.caleum.cadastro.model.Aluno;
import br.com.caleum.cadastro.util.Localizador;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaFragment extends SupportMapFragment{
	
	@Override
	public void onResume() {
		super.onResume();
		Context context = getActivity();
		
		AlunoDAO dao = new AlunoDAO(context);
		List<Aluno> lista = dao.getLista();
		
		for (Aluno aluno : lista) {
			GoogleMap map = getMap();
			LatLng localAluno = new Localizador(context).getCoordenada(aluno.getEndereco());
			MarkerOptions marcador = new MarkerOptions().title(aluno.getNome()).position(localAluno);
			map.addMarker(marcador);
		}
		
		dao.close();
		
		
	}

	public void centralizaLocal(LatLng local) {
		GoogleMap map = getMap();
		CameraUpdate update = CameraUpdateFactory.newLatLngZoom(local, 15);
		map.animateCamera(update);
	}

}
