package br.com.caleum.cadastro.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.caleum.cadastro.model.Aluno;

public class AlunoDAO extends SQLiteOpenHelper{

	String[] COLLUNS = {"id", "nome", "endereco", "telefone", "site", "foto", "nota"};
	private static final String DATABASE = "CadastroCaelum";
	private static final int VERSAO_1 = 2;

	public AlunoDAO(Context context) {
		super(context, DATABASE, null, VERSAO_1);
	}

	

	@Override
	public void onCreate(SQLiteDatabase db) {
		String ddl = "CREATE TABLE Aluno (id INTEGER PRIMARY KEY,"
				+ "nome TEXT UNIQUE NOT NULL,"
				+ "telefone TEXT,"
				+ "endereco TEXT,"
				+ "site TEXT,"
				+ "foto TEXT,"
				+ "nota REAL);";
		db.execSQL(ddl);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String dll = "DROP TABLE IF EXISTS Aluno";
		db.execSQL(dll);
		
		this.onCreate(db);
	}
	
	public void salva(Aluno aluno) {
		ContentValues values = toValues(aluno);
		getWritableDatabase().insert("Aluno", null, values);
	}



	private ContentValues toValues(Aluno aluno) {
		ContentValues values = new ContentValues();
		values.put("nome", aluno.getNome());
		values.put("telefone", aluno.getTelefone());
		values.put("endereco", aluno.getEndereco());
		values.put("site", aluno.getSite());
		values.put("foto", aluno.getFoto());
		values.put("nota", aluno.getNota());
		return values;
	}

	public List<Aluno> getLista() {
		Cursor cursor = getWritableDatabase().query("Aluno", COLLUNS, null, null, null, null, null);
		
		List<Aluno> alunosCadastrados = new ArrayList<Aluno>(); 
		while (cursor.moveToNext()) {
			Aluno aluno = new Aluno();
			aluno.setId(cursor.getLong(0));
			aluno.setNome(cursor.getString(1));
			aluno.setEndereco(cursor.getString(2));
			aluno.setTelefone(cursor.getString(3));
			aluno.setSite(cursor.getString(4));
			aluno.setFoto(cursor.getString(5));
			aluno.setNota(cursor.getDouble(6));
			alunosCadastrados.add(aluno);
 		}
		return alunosCadastrados;
	}



	public void deletar(Aluno aluno) {
		String[] args = {aluno.getId().toString()};
		getWritableDatabase().delete("Aluno", "id=?", args);
	}



	public void edita(Aluno aluno) {
		ContentValues values = toValues(aluno);
		String[] args = {aluno.getId().toString()};
		getWritableDatabase().update("Aluno", values, "id=?", args);
	}



	public boolean isAluno(String telefone) {
		String[] args = {telefone};
		Cursor cursor = getWritableDatabase().rawQuery("select id from Aluno where telefone = ?", args);
		boolean existeUmPrimeiro = cursor.moveToFirst();
		return existeUmPrimeiro;
		
	}

}
