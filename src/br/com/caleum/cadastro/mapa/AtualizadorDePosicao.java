package br.com.caleum.cadastro.mapa;

import com.google.android.gms.maps.model.LatLng;

import br.com.caleum.cadastro.fragment.MapaFragment;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class AtualizadorDePosicao implements LocationListener {
	
	private LocationManager locationManager;
	private MapaFragment mapa;

	public AtualizadorDePosicao(Activity activity, MapaFragment mapa){
		this.mapa = mapa;
		locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		
		String provider = LocationManager.GPS_PROVIDER;
		long minTime = 20000; //ms
		float minDistance = 20; //m
		
		locationManager.requestLocationUpdates(provider, minTime, minDistance, this);
	}
	
	public void cancelar() {
		locationManager.removeUpdates(this);
	}

	@Override
	public void onLocationChanged(Location novaLocation) {
		LatLng local = new LatLng(novaLocation.getLatitude(), novaLocation.getLongitude());
		mapa.centralizaLocal(local);
		
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	

}
