package br.com.caleum.cadastro.task;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import br.com.caleum.cadastro.converter.AlunoConverter;
import br.com.caleum.cadastro.dao.AlunoDAO;
import br.com.caleum.cadastro.model.Aluno;
import br.com.caleum.cadastro.support.WebClient;

public class EnviaAlunoTask extends AsyncTask<Integer, Double, String>{

	private Context context;
	private ProgressDialog progress;
	
	public EnviaAlunoTask(Context context){
		this.context = context;
	}

	
	@Override
	protected void onPreExecute() {
		progress = ProgressDialog.show(context, "Aguarde...", "Enviando dados para web...",true, true);
	}
	
	@Override
	protected String doInBackground(Integer... params) {
		
		String urlServer = "http://www.caelum.com.br/mobile";
		AlunoDAO alunoDAO = new AlunoDAO(context);
		List<Aluno> listaDeAlunos = alunoDAO.getLista();
		alunoDAO.close();
		
		
		String dadosJSON = new AlunoConverter().toJSON(listaDeAlunos);
		
		WebClient client = new WebClient(urlServer);
		final String resposta = client.post(dadosJSON);

		
		return resposta;
		
	}
	
	@Override
	protected void onPostExecute(String result) {
		progress.dismiss();
		Toast.makeText(context, result, Toast.LENGTH_LONG).show();
	}

	

}
