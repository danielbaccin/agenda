package br.com.caleum.cadastro.helper;

import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.R.id;
import br.com.caleum.cadastro.activity.FormularioActivity;
import br.com.caleum.cadastro.model.Aluno;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Toast;

public class FormularioActivityHelper {

	private EditText editNome;
	private EditText editSite;
	private EditText editTelefone;
	private EditText editEndereco;
	private SeekBar seekNota;
	private ImageView foto;
	private Aluno aluno;

	public FormularioActivityHelper(FormularioActivity formularioActivity) {
		editNome = (EditText) formularioActivity.findViewById(R.id.nome);
		editSite = (EditText) formularioActivity.findViewById(R.id.site);
		editTelefone = (EditText) formularioActivity.findViewById(R.id.telefone);
		editEndereco = (EditText) formularioActivity.findViewById(R.id. endereco);
		seekNota = (SeekBar) formularioActivity.findViewById(R.id.nota);
		foto = (ImageView) formularioActivity.findViewById(R.id.foto);
		aluno = new Aluno();
	}

	public Aluno obtemAlunoDoFormulario() {
		
		aluno.setNome(editNome.getText().toString());
		aluno.setSite(editSite.getText().toString());
		aluno.setTelefone(editTelefone.getText().toString());
		aluno.setEndereco(editEndereco.getText().toString());
		aluno.setNota(Double.valueOf(seekNota.getProgress()));
		
		return aluno;
	}

	public void populaFormularioComAluno(Aluno alunoParaEdicao) {
		aluno = alunoParaEdicao;
		editNome.setText(alunoParaEdicao.getNome());
		editSite.setText(alunoParaEdicao.getSite());
		editTelefone.setText(alunoParaEdicao.getTelefone());
		editEndereco.setText(alunoParaEdicao.getEndereco());
		seekNota.setProgress(alunoParaEdicao.getNota().intValue());
		
		if(aluno.getFoto() != null){
			carregaImagem(alunoParaEdicao.getFoto());
		}
	}
	
	 public ImageView getFoto() {
		return foto;
	}

	public void carregaImagem(String caminhoParaArquivo) {
		aluno.setFoto(caminhoParaArquivo);
		
		Bitmap imagem = BitmapFactory.decodeFile(caminhoParaArquivo);
		if(imagem != null){
			Bitmap imagemReduzida = Bitmap.createScaledBitmap(imagem, 100, 100, true);
			foto.setImageBitmap(imagemReduzida);
		}
	}

}
 