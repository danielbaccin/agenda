package br.com.caleum.cadastro.converter;

import java.util.List;

import org.json.JSONException;
import org.json.JSONStringer;

import br.com.caleum.cadastro.model.Aluno;

public class AlunoConverter {

	public String toJSON(List<Aluno> listaDeAlunos) {
		try {
		
			JSONStringer js = new JSONStringer();

			js.object().key("list").array();
				js.object().key("aluno").array();
					for (Aluno aluno : listaDeAlunos) {
						js.object();
//							js.key("id").value(aluno.getId());
							js.key("nome").value(aluno.getNome());
//							js.key("endereco").value(aluno.getEndereco());
//							js.key("telefone").value(aluno.getTelefone());
//							js.key("site").value(aluno.getSite());
							js.key("nota").value(aluno.getNota());
						js.endObject();
					}
				js.endArray().endObject();
			js.endArray().endObject().toString();
				
			return js.toString();
		
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		
		
	}

}
