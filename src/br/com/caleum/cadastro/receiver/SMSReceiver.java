package br.com.caleum.cadastro.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.SmsMessage;
import android.widget.Toast;
import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.dao.AlunoDAO;

public class SMSReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Object[] msgs = (Object[]) intent.getExtras().get("pdus");
		
		byte[] primeira = (byte[]) msgs[0];
		
		SmsMessage sms = SmsMessage.createFromPdu(primeira);
		
		String telefone = sms.getDisplayOriginatingAddress();
		
		AlunoDAO alunoDAO = new AlunoDAO(context);
		
		boolean ehAluno = alunoDAO.isAluno(telefone);
		if(ehAluno){
			MediaPlayer mp = MediaPlayer.create(context, R.raw.msg);
			Toast.makeText(context, "Recebendo SMS", Toast.LENGTH_LONG).show();
            mp.start();
		}
		
		alunoDAO.close();
		
	}

}
