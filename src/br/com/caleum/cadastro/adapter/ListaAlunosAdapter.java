package br.com.caleum.cadastro.adapter;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.caleum.cadastro.R;
import br.com.caleum.cadastro.model.Aluno;

public class ListaAlunosAdapter extends BaseAdapter{
	
	private final List<Aluno> alunos;
	private final Activity activity;
	
	
	public ListaAlunosAdapter(Activity activity, List<Aluno> alunos){
		this.activity = activity;
		this.alunos = alunos;
	}

	@Override
	public int getCount() {
		return alunos.size();
	}

	@Override
	public Object getItem(int position) {
		return alunos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return alunos.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View linha = activity.getLayoutInflater().inflate(R.layout.item, null);
		
		if (position % 2 == 0) {
			linha.setBackgroundColor(activity.getResources().
                getColor(R.color.linha_par));
        }else{
        	linha.setBackgroundColor(activity.getResources().
                    getColor(R.color.linha_impar));
        }
        	
		
		Aluno aluno  = alunos.get(position);
		TextView nome = (TextView) linha.findViewById(R.id.nome);
		nome.setText(aluno.getNome());
		
		ImageView foto = (ImageView) linha.findViewById(R.id.foto);
		if(aluno.getFoto() != null){
			Bitmap fotoDoAluno = BitmapFactory.decodeFile(aluno.getFoto());
			Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoDoAluno, 100, 100, true);
			foto.setImageBitmap(fotoReduzida);
		}else{
			Drawable semFoto = activity.getResources().getDrawable(R.drawable.ic_no_image);
			foto.setImageDrawable(semFoto);
		}
		
		TextView telefone = (TextView) linha.findViewById(R.id.telefone);
		if(telefone!=null)
			telefone.setText(aluno.getTelefone());
		
		TextView site = (TextView) linha.findViewById(R.id.site);
		if(site!=null)
			site.setText(aluno.getSite());
		
		
		
 		return linha;
	}
	
	

}
